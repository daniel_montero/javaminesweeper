package game;

public enum ButtonState {

    HIDDEN, MINE, MARKED, BLANK, MARKED_WRONG;

}
