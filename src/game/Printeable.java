/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author danielmontero
 */
public interface Printeable {

    void printTimer();

    void printMineCounter(int mineCounter);

    void resetTime();

}
