package game.util;

import graphics.MSButton;

import java.util.ArrayList;

public class ClickEventManager {

    private static ClickEventManager _instance = new ClickEventManager();

    private static ClickEventManager getInstance() {
        return _instance;
    }
    private ArrayList<MSButton> list;

    private ClickEventManager() {
        list = new ArrayList<MSButton>();
    }

    public static boolean addToList(MSButton button) {
        if (!getInstance().list.contains(button)) {
            getInstance().list.add(button);
            return true;
        }
        return false;
    }

    public static void dispatchEvents() throws GameOverException {
        for (MSButton button : getInstance().list) {

            button.onClick();
            button.repaint();
        }
    }

    public static void clear() {
        getInstance().list.clear();
    }

}
