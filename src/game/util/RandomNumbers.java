package game.util;

import java.util.ArrayList;
import java.util.Random;

public class RandomNumbers {

    private static final Random random = new Random();

    public static int getRandomInteger(int min, int max) {
        return min + random.nextInt(max - min);
    }

    public static int getRandomInteger(int max) {
        return random.nextInt(max);
    }

    public static ArrayList<Pair> getIterativeVectorPair(int x, int y) {
        ArrayList<Pair> vtr = new ArrayList<Pair>();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                vtr.add(new Pair(i, j));
            }
        }
        return vtr;

    }

}
