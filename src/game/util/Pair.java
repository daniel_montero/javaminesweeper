package game.util;

public class Pair {

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pair) {
            Pair pr = (Pair) obj;
            return pr.x == x && pr.y == y;
        }
        return false;
    }

    public int x, y;
}
