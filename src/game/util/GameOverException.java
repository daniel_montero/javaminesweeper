package game.util;

public class GameOverException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public GameOverException() {
        // TODO Auto-generated constructor stub
    }

    public GameOverException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public GameOverException(Throwable arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public GameOverException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

}
