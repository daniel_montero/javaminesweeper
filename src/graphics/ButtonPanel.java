package graphics;

import game.ButtonState;
import game.Printeable;
import game.util.ClickEventManager;
import game.util.GameOverException;
import game.util.Pair;
import game.util.RandomNumbers;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

public class ButtonPanel extends JPanel {

    /**
     *
     */
    private int width, height;
    private static final long serialVersionUID = 1L;
    private MSButton[][] buttonArray;
    public static int MINE_NUMBER = 10;
    private Printeable printer;
    Timer timer;

    public ButtonPanel(Printeable printer, int width, int height) {
        super();
        this.printer = printer;
        setArrayWidth(width);
        setArrayHeight(height);

        init(true);

    }
    private MyMouseEventListener mouseListener;

    public void init() {
        init(false);
    }

    private class TimerActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            printer.printTimer();
        }

    }

    private void init(boolean firstTime) {

        if (timer == null) {
            timer = new Timer(100, new TimerActionListener());
            timer.start();
        } else {
            timer.restart();
        }
        if (mouseListener == null) {
            mouseListener = new MyMouseEventListener();
        }
        if (buttonArray == null) {
            buttonArray = new MSButton[width][height];
        }
        int buttonLenght = 40;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (buttonArray[i][j] == null) {
                    buttonArray[i][j] = new MSButton(i, j);
                } else {
                    buttonArray[i][j].reset();
                    isInit = false;
                }
                if (firstTime) {
                    buttonArray[i][j].addMouseListener(mouseListener);
                    add(buttonArray[i][j]);
                    buttonArray[i][j].setBounds(i * buttonLenght, j * buttonLenght,
                            (int) (buttonLenght * 0.96),
                            (int) (buttonLenght * 0.96));
                    buttonArray[i][j].setVisible(true);
                }
            }
        }

    }

    public static boolean isInit = false;

    private class MyMouseEventListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (!e.isControlDown()) {
                MSButton msbutton = (MSButton) (e.getSource());
                if (!isInit) {
                    fillMinePanel(msbutton.x, msbutton.y);
                    isInit = true;
                }
                try {
                    msbutton.onClick();
                    if (msbutton.getMineCount() == 0) {
                        ClickEventManager.clear();
                        clickAllNeighbors(msbutton.x, msbutton.y);
                        ClickEventManager.dispatchEvents();
                    }
                } catch (GameOverException e1) {
                    onGameOver();
                }
            } else {

                MSButton msbutton = (MSButton) (e.getSource());
                msbutton.onRightClick();
            }
            printer.printMineCounter(getMineLeftCount());

        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub

        }

        @Override
        public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub

        }

        @Override
        public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

        }

    }

    private void onGameOver() {
        timer.stop();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                if (buttonArray[i][j].isMine()) {
                    if (buttonArray[i][j].buttonState != ButtonState.MARKED) {
                        buttonArray[i][j].buttonState = ButtonState.MINE;
                    }
                } else {
                    if (buttonArray[i][j].buttonState == ButtonState.MARKED) {
                        buttonArray[i][j].buttonState = ButtonState.MARKED_WRONG;
                    } else {
                        buttonArray[i][j].buttonState = ButtonState.BLANK;
                    }
                }
                buttonArray[i][j].repaint();
            }
        }

    }

    private void plusOneNeighbors(int x, int y) {
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && i < width && j >= 0 && j < height) {
                    buttonArray[i][j].plusOne();
                }
            }
        }
    }

    private int getMineLeftCount() {
        int counter = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (i >= 0 && i < width && j >= 0 && j < height) {
                    if (buttonArray[i][j].buttonState == ButtonState.MARKED) {
                        counter++;
                    }
                }
            }
        }
        return MINE_NUMBER - counter;
    }

    private void clickAllNeighbors(int x, int y) {
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && i < width && j >= 0 && j < height) {
                    if (i == x && j == y) {
                        continue;
                    }
                    if (buttonArray[i][j].buttonState == ButtonState.MARKED) {
                        continue;
                    }
                    if (ClickEventManager.addToList(buttonArray[i][j])
                            && buttonArray[i][j].getMineCount() == 0) {
                        clickAllNeighbors(i, j);
                    }
                }
            }
        }

    }

    private void fillMinePanel(int x, int y) {
        ArrayList<Pair> vtr = RandomNumbers.getIterativeVectorPair(width, height);
        Pair pair;
        printer.resetTime();
        printer.printMineCounter(getMineLeftCount());

        vtr.remove(new Pair(x, y));
        for (int i = 0; i < MINE_NUMBER; i++) {
            pair = vtr.get(RandomNumbers.getRandomInteger(vtr.size()));
            buttonArray[pair.x][pair.y].setMine(true);

            plusOneNeighbors(pair.x, pair.y);
            vtr.remove(pair);
        }

    }

    public int getArrayWidth() {
        return width;
    }

    public void setArrayWidth(int width) {
        this.width = width;
    }

    public int getArrayHeight() {
        return height;
    }

    public void setArrayHeight(int height) {
        this.height = height;
    }

}
