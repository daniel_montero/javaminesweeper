package graphics;

import java.awt.Graphics;

import javax.swing.JButton;

import game.ButtonState;
import game.util.GameOverException;
import graphics.img.ImageHelper;

public class MSButton extends JButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    ButtonState buttonState;
    private int mineCount = 0;
    private boolean isMine = false;
    int x, y;

    public MSButton(int x, int y) {
        super();
        this.x = x;
        this.y = y;
        this.isMine = isMine;
        buttonState = ButtonState.HIDDEN;
    }

    public void setMineCount(int count) {
        mineCount = count;

    }

    public void reset() {
        setState(ButtonState.HIDDEN);
        setMine(false);
        setMineCount(0);
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(ImageHelper.getImage(buttonState), 0, 0, this);
        if (buttonState == ButtonState.BLANK) {
            g.drawImage(ImageHelper.getImage(buttonState + "" + getMineCount()), 0, 0, this);

        }

    }

    public void setMine(boolean bool) {
        isMine = bool;
    }

    public boolean isMine() {
        return isMine;
    }

    public void plusOne() {
        mineCount++;
    }

    public int getMineCount() {
        return mineCount;
    }

    public void setState(ButtonState btnS) {
        this.buttonState = btnS;
    }

    public void onClick() throws GameOverException {
        if (isMine()) {
            setState(ButtonState.MINE);
            throw new GameOverException();
        } else {
            setState(ButtonState.BLANK);

        }
    }

    public void onRightClick() {
        if (buttonState == ButtonState.HIDDEN) {
            setState(ButtonState.MARKED);
        } else if (buttonState == ButtonState.MARKED) {
            setState(ButtonState.HIDDEN);
        }

    }

}
