package graphics.img;

import game.ButtonState;

import java.awt.Image;
import java.util.HashMap;

import javax.swing.ImageIcon;

public class ImageHelper {

    private final HashMap<Object, Image> imageMap;
    private static ImageHelper imgHelperInstance;

    private static ImageHelper getInstance() {
        if (imgHelperInstance == null) {
            imgHelperInstance = new ImageHelper();
            imgHelperInstance.init();
        }
        return imgHelperInstance;

    }

    public static String getImagePath() {
        return "/graphics/img/";
    }

    private ImageHelper() {

        imageMap = new HashMap<Object, Image>();
    }

    private void init() {
        add(ButtonState.HIDDEN, "hidden.png");
        add(ButtonState.MARKED, "marked.png");
        add(ButtonState.MINE, "mine.png");
        add(ButtonState.BLANK, "blank.png");
        add(ButtonState.MARKED_WRONG, "marked_wrong.png");
        add("icon", "ico.png");
        add("BLANK0", "n0.png");
        add("BLANK1", "n1.png");
        add("BLANK2", "n2.png");
        add("BLANK3", "n3.png");
        add("BLANK4", "n4.png");
        add("BLANK5", "n5.png");
        add("BLANK6", "n6.png");
        add("BLANK7", "n7.png");
        add("BLANK8", "n8.png");

    }

    public static void add(Object obj, String name) {
        Image img;
        img = new ImageIcon(getInstance().getClass().getResource(
                getImagePath() + name)).getImage();
        getInstance().imageMap.put(obj, img);

    }

    public static Image getImage(Object obj) {

        return getInstance().imageMap.get(obj);
    }

}
